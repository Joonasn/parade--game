﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace ParaatiOpen
{
    class TitleScreen
    {
        protected Texture2D[] logoText = new Texture2D[6];
        protected Texture2D logoBG;
        protected SpriteFont Font;
        protected Rectangle introBGAnim, introBGSize;
        protected int introBGAnimMax, introBGWidth;
        protected Rectangle[] introTextLocation = new Rectangle[6];
        protected Rectangle[] introTextAnimBegin = new Rectangle[6];
        protected int[] introTextframe = new int[6];
        protected float timer, delay = 200f;
        protected Vector2 CreatorText, CommandText;

        private static ContentManager content; // Get data from Main -class
        public static ContentManager Content
        {
            protected get { return content; }
            set { content = value; }
        }

        public TitleScreen (SpriteFont font, int SW, int SH) //Setup
        {
            Font = font;
            logoBG = Content.Load<Texture2D>("introbg");

            for (int i = 0; i < 6; i++)
            {
                logoText[i] = Content.Load<Texture2D>("logo" + i);
            }

            introBGSize = new Rectangle(((SW - 600) / 2), ((SH - 120) / 2)-240, 600, 120); //Adjust positions based on screen width(SW) and height(SH)
            introBGAnim = introBGSize;
            introBGWidth = (logoBG.Width / 3);
            CreatorText = new Vector2(introBGSize.X + 170, introBGSize.Y + 120);
            CommandText = new Vector2((SW / 2) - 120, ((SH / 2) - 10));

            for (int i = 0; i < logoText.Length; i++) // Adjust text
            {
                introTextLocation[i] = new Rectangle((introBGSize.X+30) + (i * 90), (introBGSize.Y+10), 90, 100);
            }
        }

        public void Update (GameTime gametime) // Handle Animations 
        {
            timer += (float)gametime.ElapsedGameTime.TotalMilliseconds; 
            if (timer >= delay)
            {
                introBGAnim = new Rectangle(introBGWidth * introBGAnimMax, 0, introBGWidth, logoBG.Height); // Change animation frame
                introBGAnimMax = (introBGAnimMax + 1) % 3;

                for (int i = 0; i < logoText.Length; i++)
                {
                    introTextAnimBegin[i] = new Rectangle(45 * introTextframe[i], 0, 44, 50);  // Current image, frame 0/"light" -frame
                    introTextframe[i] = 0;

                    for (int j = 0; j < logoText.Length; j++) // Other images play frame number 1/ "Shadow" -frame
                    {
                        if (introTextAnimBegin[j] != introTextAnimBegin[i])  
                        {
                            introTextframe[j] = 1;
                        }
                    }
                }
                timer = 0; // Reset timer
            }
        }

        public void Draw (SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(logoBG, introBGSize, introBGAnim, Color.White);

            for (int i = 0; i < logoText.Length; i++)
            {
                spriteBatch.Draw(logoText[i], introTextLocation[i], introTextAnimBegin[i], Color.White);
            }
            spriteBatch.DrawString(Font, "SopuisaSopuli, 2018-2019", new Vector2(CreatorText.X+3, CreatorText.Y+3), Color.Black);
            spriteBatch.DrawString(Font, "SopuisaSopuli, 2018-2019", new Vector2(CreatorText.X, CreatorText.Y), Color.White);
            spriteBatch.DrawString(Font, "Hit enter to start!", new Vector2(CommandText.X+3, CommandText.Y+3), Color.Black);
            spriteBatch.DrawString(Font, "Hit enter to start!", new Vector2(CommandText.X, CommandText.Y), Color.White);
        }
    }
}