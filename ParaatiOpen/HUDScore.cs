﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace ParaatiOpen
{
    class HUDScore
    {
        protected SpriteFont Font;
        protected Texture2D GameOverImage;
        protected Rectangle GameOverImageLocation, GameOverAnimation;
        protected Color LerpingColors;
        protected Color lerp1 = new Color(255, 255, 255);
        protected Color lerp2 = new Color(255, 255, 255);
        protected Random ColorChange = new Random();
        protected int red, green, blue, GameoverDelay = 0, GameOverFrame = 0, FrameDivider = 2;
        protected float GameoverTimer = 0, TimerDelay = 200;
        protected string[] PauseText = new string[] { "-PAUSE-", "WASD/Arrow-keys = Walk", "Space = Use item", "F = Toggle screen size","", "M = Toggle Audio", "ESC = End game" };
        protected string[] ScoreText = new string[] { "", "", "HI-SCORE!!!", "Hit enter to try again!"};
        protected Vector2 AdjustedTextLocation;
        protected Vector2[,] AdjustedTextYX;
        protected Vector2 HUDLocation;
    
        private static ContentManager content;
        public static ContentManager Content
        {
            protected get { return content; }
            set { content = value; }
        }

        public HUDScore(SpriteFont font, int SW, int SH) // Setup
        {
            Font = font;
            GameOverImage = Content.Load<Texture2D>("gameover");
            GameOverImageLocation = new Rectangle(((SW - 600) / 2), ((SH - 120) / 2) - 240, 587, 118);
            GameOverAnimation = GameOverImageLocation;
            AdjustedTextYX = new Vector2[PauseText.Length, PauseText.Length];
            AdjustedTextLocation = new Vector2((SW / 2), (SH / 2) - 150);
            HUDLocation = new Vector2((SW / 2) -230, (SH / 2) - 390);
            AdjustText(PauseText, AdjustedTextYX);
        }

        protected void AdjustText(string[] Text, Vector2[,] Location) // Center text based on text length 
        {
            for (int i = 0; i < Text.Length; i++)
            {
                Location[1, i] = Font.MeasureString(Text[i]);
                Location[0, i] = new Vector2(Font.MeasureString(Text[i]).X / 2, (Location[1, i].Y / 2) - 50 * i);
            }
        }

        public void ResetHUD() // Reset values and adjust text for pause screen
        {
            GameoverTimer = 0;
            GameoverDelay = 0;
            AdjustText(PauseText, AdjustedTextYX); // Overwrites text location values of "Gameover screen". 
        }

        protected void ColorLerpOn() //Color lerping for "HI-Score" and "Items" texts.
        {
            red = ColorChange.Next(256);  // Select random color from values 0-255
            green = ColorChange.Next(256);
            blue = ColorChange.Next(256);
            lerp1 = new Color(red, green, blue); // Create color from random values
            LerpingColors = Color.Lerp(lerp1, lerp2, 0.5f); // Create color lerp
        }

        public void UpdateGameOver(GameTime gameTime) // Gameover, Draw stuff with delay
        {
            ColorLerpOn();
            GameoverTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (GameoverTimer >= TimerDelay)  
            {
                GameOverAnimation = new Rectangle(587 * GameOverFrame, 0, 587, 118); // Change frames
                GameOverFrame = (GameOverFrame + 1) % FrameDivider;
                
                if (GameoverDelay <2)
                {
                    GameoverDelay++;
                }
                GameoverTimer = 0;
            }
        }

        public void DrawStats(SpriteBatch spriteBatch, int Score, int HiScore, string HoldText) //Ingame "score -bar"
        {
            ColorLerpOn();
            spriteBatch.DrawString(Font, "P: " + Score.ToString("0000000"), new Vector2(HUDLocation.X, HUDLocation.Y + 13), Color.Black);
            spriteBatch.DrawString(Font, "P: " + Score.ToString("0000000"), new Vector2(HUDLocation.X, HUDLocation.Y + 10), Color.White);
            spriteBatch.DrawString(Font, HoldText, new Vector2(HUDLocation.X + 200, HUDLocation.Y + 13), Color.Black);
            spriteBatch.DrawString(Font, HoldText, new Vector2(HUDLocation.X + 200, HUDLocation.Y + 10), LerpingColors);
            spriteBatch.DrawString(Font, "--------", new Vector2(HUDLocation.X + 200, HUDLocation.Y + 33), Color.Black);
            spriteBatch.DrawString(Font, "--------", new Vector2(HUDLocation.X + 200, HUDLocation.Y + 30), Color.White);
            spriteBatch.DrawString(Font, "H: " + HiScore.ToString("0000000"), new Vector2(HUDLocation.X + 330, HUDLocation.Y + 13), Color.Black);
            spriteBatch.DrawString(Font, "H: " + HiScore.ToString("0000000"), new Vector2(HUDLocation.X + 330, HUDLocation.Y + 10), Color.White);
        }

        public void DrawPause(SpriteBatch spriteBatch) // Pause text
        {
            for (int i= 0; i < PauseText.Length; i++)
            {
                spriteBatch.DrawString(Font, PauseText[i], new Vector2((int)AdjustedTextLocation.X - AdjustedTextYX[0, i].X+3, (int)AdjustedTextLocation.Y - AdjustedTextYX[0, i].Y+3), Color.Black);
                spriteBatch.DrawString(Font, PauseText[i], new Vector2((int)AdjustedTextLocation.X - AdjustedTextYX[0, i].X, (int)AdjustedTextLocation.Y - AdjustedTextYX[0, i].Y), Color.White); 
            }
        }

        public void DrawGameOver(SpriteBatch spriteBatch, int Score, int HiScore, bool ShowHighScore) // Gameover text
        {
            if (GameoverDelay == 0)
            {
                ScoreText[1] = "Final score: " + Score;  // Adjust Score texts for Gameover screen
                AdjustText(ScoreText, AdjustedTextYX);
            }

            if (GameoverDelay >= 1)
            {
                spriteBatch.Draw(GameOverImage, GameOverImageLocation, GameOverAnimation, Color.White); // Show "Gameover" -image
            }

            if (GameoverDelay >= 2)
            {
                for (int i = 0; i < ScoreText.Length; i++)
                {
                    if (i != 2)  // Show "Final Score" and "Try again" -texts
                    {
                        spriteBatch.DrawString(Font, ScoreText[i], new Vector2((int)AdjustedTextLocation.X - AdjustedTextYX[0, i].X + 3, (int)AdjustedTextLocation.Y - AdjustedTextYX[0, i].Y + 3), Color.Black);
                        spriteBatch.DrawString(Font, ScoreText[i], new Vector2((int)AdjustedTextLocation.X - AdjustedTextYX[0, i].X, (int)AdjustedTextLocation.Y - AdjustedTextYX[0, i].Y), Color.White);
                    }
                    if (ShowHighScore) // If player got Hi- score, show following
                    {
                        spriteBatch.DrawString(Font, ScoreText[2], new Vector2((int)AdjustedTextLocation.X - AdjustedTextYX[0, 2].X + 3, (int)AdjustedTextLocation.Y - AdjustedTextYX[0, 2].Y + 3), Color.Black);
                        spriteBatch.DrawString(Font, ScoreText[2], new Vector2((int)AdjustedTextLocation.X - AdjustedTextYX[0, 2].X, (int)AdjustedTextLocation.Y - AdjustedTextYX[0, 2].Y), LerpingColors);
                    }
                }
            }
        }
    }
}