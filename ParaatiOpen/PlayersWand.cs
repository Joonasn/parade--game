﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ParaatiOpen
{
    class PlayersWand
    {
        protected Texture2D WandImage;
        public Rectangle WandCollision;
        protected Rectangle WandAnimate;
        protected int TextureWidth, TextureHeight, CurrentFrame, MaxFrames, frameCycle, Framedalay = 4;

        public PlayersWand(Texture2D image, int maxAnim)
        {
            WandImage = image;
            WandCollision = new Rectangle(0, -150, ((image.Width / maxAnim) / 3), (image.Height / 3));
            TextureWidth = (image.Width / maxAnim);
            TextureHeight = image.Height;
            MaxFrames = maxAnim;
        }

        public virtual void ResetPosition(int LocationY, int LocationX)
        {
            WandCollision.Y = LocationY;
            WandCollision.X = LocationX;
        }

        public virtual void Update(int GameSpeed)
        {
            if (WandCollision.X > -100)
            {
                WandCollision.X -= (3 + GameSpeed);
                Animation();
            }
            else // Reset
            {
                ResetPosition(-150, 0);
            }
        }

        public virtual void Animation()
        {
            WandAnimate = new Rectangle(TextureWidth * CurrentFrame, 0, TextureWidth, TextureHeight);
            frameCycle++;
            if (frameCycle >= Framedalay)
            {
                CurrentFrame = (CurrentFrame + 1) % MaxFrames;
                frameCycle = 0;
            }
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(WandImage, WandCollision, WandAnimate, Color.White);
        }
    }
}