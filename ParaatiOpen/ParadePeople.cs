﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace ParaatiOpen
{
    class ParadePeople : ICloneable
    {
        private Texture2D[] image = new Texture2D[8];
        private Rectangle[] Collision = new Rectangle[8];
        protected Texture2D paradePerson;
        public Rectangle personSize;
        protected Rectangle PersonAnimate;
        protected int Speed, Points, CurrentFrame, MaxFrames, UnlockOnSpeed, CurrentLock,LockFromScreen, AppearRate, AppearRateMax, TextureWidth, TextureHeight;
        protected int frameCycle, CurrentlyOnScreen, Framedalay=4, addSpeed=0, LastFrame=0, newLocation=200;

        private static ContentManager content;
        public static ContentManager Content
        {
            protected get { return content; }
            set { content = value; }
        }

        public ParadePeople(Texture2D image, int newSpeed, int newPoints, int maxAnim, int newunlock, int newLock, int maxappear)
        {
            paradePerson = image;
            TextureWidth = (image.Width / maxAnim);
            TextureHeight = image.Height;
            personSize = new Rectangle(200, 0, (TextureWidth / 3), (TextureHeight / 3));
            Speed = newSpeed;
            Points = newPoints;
            MaxFrames = maxAnim;
            UnlockOnSpeed = newunlock;
            LockFromScreen = newLock;
            CurrentLock = LockFromScreen;
            AppearRate = newLock;
            AppearRateMax = maxappear;
        }

        public virtual int ReturnCurrentLock()
        {
            return CurrentLock;
        }

        public virtual void ResetStatus()
        {
            personSize.X = -250;
            CurrentLock = LockFromScreen;
        }

        public virtual void Update(int GameSpeed, GraphicsDevice graphics, GameTime gameTime)
        {
            addSpeed = (Speed + GameSpeed);

            if (CurrentLock <= 0) // Animation
            {
                Animation(addSpeed);
                Movement(addSpeed);
                OutFromScreen(graphics);
            }
        }

        public virtual void Movement(int CurrentSpeed)
        {
            personSize.X += CurrentSpeed;
        }

        public virtual void Animation(int Speed)
        {
            PersonAnimate = new Rectangle(TextureWidth * CurrentFrame, 0, TextureWidth, TextureHeight);
            frameCycle++;
            if (frameCycle >= Framedalay)
            {
                CurrentFrame = (CurrentFrame + 1) % (MaxFrames - LastFrame);
                frameCycle = 0;
            }
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }


        public virtual void OutFromScreen (GraphicsDevice graphics)
        {
            if (personSize.X > graphics.Viewport.Width || personSize.X < -60)
            {
                ResetStatus();
            }
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(paradePerson, personSize, PersonAnimate, Color.White);
        }
    } 

    class Items: ParadePeople
    {
        protected Random UnlockItem = new Random();
        public Items(Texture2D image, int newSpeed, int newPoints, int maxAnim, int newunlock, int newLock, int maxappear)
        : base(image, newSpeed, newPoints, maxAnim, newunlock, newLock, maxappear)
        {
            TextureWidth = (image.Width / maxAnim);
            TextureHeight = image.Height;
            personSize = new Rectangle(200, 0, (TextureWidth / 2), (TextureHeight / 2));
            Framedalay = 50;
        }

        public virtual void Unlock(int GameSpeed, int SpeedRise)
        {
            if (GameSpeed > UnlockOnSpeed && SpeedRise >= AppearRateMax && CurrentLock >0)
            {
                newLocation = UnlockItem.Next(200, 650);
                CurrentLock = 0;
                personSize.Y = newLocation;
            }
        }
    }

    class SpecialItem : Items
    {
        public SpecialItem(Texture2D image, int newSpeed, int newPoints, int maxAnim, int newunlock, int newLock, int maxappear)
        : base(image, newSpeed, newPoints, maxAnim, newunlock, newLock, maxappear)
        {

        }

        public override void Unlock(int XLocation, int YLocation)
        {
            if (CurrentLock >0)
            {
                personSize.X = XLocation;
                personSize.Y = YLocation;
                CurrentLock = 0;
            }
        }
    }

    class ThrowAbleWand: ParadePeople
    {
        private SoundEffect SFXenemy;
        public ThrowAbleWand(Texture2D image, int newSpeed, int newPoints, int maxAnim, int newunlock, int newLock, int maxappear)
        : base(image, newSpeed, newPoints, maxAnim, newunlock, newLock, maxappear)
        {
            personSize = new Rectangle(200, 0, (TextureWidth / 2), (TextureHeight / 2));
            LastFrame = 0;
            SFXenemy = Content.Load<SoundEffect>("aani4");
        }

        public virtual void Unlock(int newLocationY, int newLocationX)
        {
            personSize.Y = newLocationY;
            personSize.X = newLocationX;
            CurrentLock = 0;
        }

        public override void Movement(int CurrentSpeed)
        {
            personSize.X -= CurrentSpeed;
        }

        public virtual void Collision()
        {
            SFXenemy.Play();
            ResetStatus();
        }
    }

    class ParadeFolk: ParadePeople // Regular Enemies, no player needed for act.
    {
        public int Waiting=0;
        public bool isDamageTaken = false, ReturnScore = false;
        protected int AliveTimer;
        public Rectangle personCollision;
        public ParadeFolk(Texture2D image, int newSpeed, int newPoints, int maxAnim, int newunlock, int newLock, int maxappear)
        : base(image, newSpeed, newPoints, maxAnim, newunlock, newLock, maxappear)
        {
            personCollision = new Rectangle(personSize.X, personSize.Y, ((personSize.Width / 2) + 30), ((personSize.Height / 2) + 30));
            LastFrame = 1;
        }

        public override void ResetStatus()
        {
            personSize.X = -250;
            CurrentLock = LockFromScreen;
            AliveTimer = 0;
            Waiting = 0;
            isDamageTaken = false;
            personCollision.X = (personSize.X + ((personSize.Width / 3) - 10));
            personCollision.Y = (personSize.Y + (personSize.Height / 3));
        }

        public virtual void Unlock(int GameSpeed, int newLocation)
        {
            if (CurrentLock > 1)
            {
                CurrentLock -= 1;
            }

            if (GameSpeed > UnlockOnSpeed && CurrentLock == 1 && CurrentlyOnScreen < AppearRateMax)
            {
                CurrentLock = 0;
                CurrentlyOnScreen += AppearRate;
                personSize.Y = newLocation;
            }
        }

        public override void Update(int GameSpeed, GraphicsDevice graphics, GameTime gameTime)
        {
            addSpeed = (Speed + GameSpeed);
            personCollision.X = (personSize.X + ((personSize.Width / 3)-10));
            personCollision.Y = (personSize.Y + (personSize.Height /3));

            if (CurrentLock <= 0) // Animation
            {
                if (isDamageTaken)
                {
                    CurrentFrame = (MaxFrames - LastFrame);
                    AliveTimer++;
                }
                else
                {
                    Movement(addSpeed);
                }
                Animation(addSpeed);
                OutFromScreen(graphics);
            }
        }

        public override void OutFromScreen(GraphicsDevice graphics)
        {
            if (personSize.X > graphics.Viewport.Width || AliveTimer > 30)
            {
                ReturnScore = true;
                CurrentlyOnScreen -= AppearRate;
                ResetStatus();
            }
        }

        public virtual int returnPoints()
        {
            ReturnScore = false;
            return Points;
        }
    }

    class ItemDropper : ParadeFolk
    {
        public ItemDropper(Texture2D image, int newSpeed, int newPoints, int maxAnim, int newunlock, int newLock, int maxappear)
        : base(image, newSpeed, newPoints, maxAnim, newunlock, newLock, maxappear)
        {

        }

        public virtual void UpdateItemDrop(List<ParadeFolk> ParadeList)
        {

        }
    }

    class Squirrel : ItemDropper
    {
        public Wand SquirrelsWand;
        public Squirrel(Texture2D image, int newSpeed, int newPoints, int maxAnim, int newunlock, int newLock, int maxappear)
        : base(image, newSpeed, newPoints, maxAnim, newunlock, newLock, maxappear)
        {
        }

        public override void UpdateItemDrop(List<ParadeFolk> ParadeList)
        {
            if (Waiting ==2)
            {
                AddCopyWand(ParadeList);
                Waiting = 3;
            }
        }

        private void AddCopyWand (List<ParadeFolk> ParadeList)
        {
            var squirrelsWand = SquirrelsWand.Clone() as Wand;
            squirrelsWand.personSize.X = this.personSize.X;
            squirrelsWand.personSize.Y = this.personSize.Y;
            ParadeList.Add(squirrelsWand);
        }

        public override void Movement(int CurrentSpeed)
        {
            if (personSize.X >= 500)
            {
                personSize.X += 1;
                Waiting++;

                if (Waiting >= 50) 
                {
                    Waiting = 51;
                }
            }
            else
            {
                personSize.X += CurrentSpeed;
            }
        }

        public override void Animation(int AddSpeed)
        {
            PersonAnimate = new Rectangle(TextureWidth * CurrentFrame, 0, TextureWidth, TextureHeight);
            if (isDamageTaken)
            {
                CurrentFrame = MaxFrames;
            }
            else
            {
                if (personSize.X >= 500)
                {
                    if (Waiting >= 50) //Wand is not returning, play different animation
                    {
                        CurrentFrame = 8;
                    }
                    else 
                    {
                        CurrentFrame = 7;
                    }   
                }
                else 
                {
                    frameCycle++;
                    if (frameCycle >= Framedalay)
                    {
                        CurrentFrame = (CurrentFrame + 1) % (MaxFrames - 3);
                        frameCycle = 0;
                    }
                }
            }
        }
    }

    class Wand : Squirrel
    {
        protected Random WandSpawn = new Random();
        protected int DirectionThrow = 0, DirectionThrowMax = 15, Length = 0, SpeedValue = 0;
        public Wand(Texture2D image, int newSpeed, int newPoints, int maxAnim, int newunlock, int newLock, int maxappear)
        : base(image, newSpeed, newPoints, maxAnim, newunlock, newLock, maxappear)
        {
            personSize = new Rectangle(200, 0, (TextureWidth / 2), (TextureHeight / 2));
            LastFrame = 0;

            SpeedValue = -2;
            DirectionThrow = 0;
            DirectionThrowMax = -200;
            Length = WandSpawn.Next(0, 5);
            if (Length < 3)
            {
                DirectionThrowMax = -30;
            }
            CurrentLock = 0;
        }

        public override void Update(int GameSpeed, GraphicsDevice graphics, GameTime gameTime)
        {
            addSpeed = (Speed + GameSpeed);
            if (CurrentLock <= 0) // Animation
            {
                if (DirectionThrow < DirectionThrowMax)
                {
                    SpeedValue = 2;
                }
                DirectionThrow += SpeedValue;
                Movement(DirectionThrow / 5);
                Animation(addSpeed);
                OutFromScreen(graphics);
            }
        }

        public override void Movement(int CurrentSpeed)
        {
            personSize.X += CurrentSpeed;
        }

        public override void Animation(int Speed)
        {
            PersonAnimate = new Rectangle(TextureWidth * CurrentFrame, 0, TextureWidth, TextureHeight);
            frameCycle++;
            if (frameCycle >= Framedalay)
            {
                CurrentFrame = (CurrentFrame + 1) % (MaxFrames - LastFrame);
                frameCycle = 0;
            }
        }

        public override void OutFromScreen(GraphicsDevice graphics)
        {
            if (personSize.X > graphics.Viewport.Width || personSize.X < -100)
            {
                ResetStatus();
            }
        }
    }

    class FloatyGhost : ParadeFolk
    {
        public FloatyGhost(Texture2D image, int newSpeed, int newPoints, int maxAnim, int newunlock, int newLock, int maxappear)
        : base(image, newSpeed, newPoints, maxAnim, newunlock, newLock, maxappear)
        {
        }

        public override void Animation(int Speed)
        {
            PersonAnimate = new Rectangle(TextureWidth * CurrentFrame, 0, TextureWidth, TextureHeight);
            CurrentFrame = 1;
        }
        public override void Movement(int CurrentSpeed)
        {
            personSize.X += CurrentSpeed;
            personSize.Y += (int)(10 * Math.Sin((personSize.X * 0.2) * 0.5 * 2 / 20));
        }
    }
    // Classes that need players position to act.
    class WatchingPlayer : ParadeFolk // Classes that need players position to act.
    {
        protected Vector2 Getlocation;
        public WatchingPlayer(Texture2D image, int newSpeed, int newPoints, int maxAnim, int newunlock, int newLock, int maxappear) 
        :base(image, newSpeed, newPoints, maxAnim, newunlock, newLock, maxappear)
        {
            personSize = new Rectangle(200, 0, ((TextureWidth / 2) -20), (TextureHeight / 2));
        }

        public virtual void PlayerFound(int playerLocationX, int playerLocationY) //Method for Torpedo, Wand and HidingGhost enemies
        {
            Getlocation.X = playerLocationX;
            Getlocation.X = playerLocationY;
        }
    }

    class Agent : WatchingPlayer
    {
        private bool active = false;
        private float Distance;
        public Agent(Texture2D image, int newSpeed, int newPoints, int maxAnim, int newunlock, int newLock, int maxappear)
        : base(image, newSpeed, newPoints, maxAnim, newunlock, newLock, maxappear)
        {
        }

        public override void Unlock(int GameSpeed, int newLocation)
        {
            if (CurrentLock > 1)
            {
                CurrentLock -= 1;
            }

            if (GameSpeed > UnlockOnSpeed && CurrentLock == 1 && CurrentlyOnScreen < AppearRateMax)
            {
                personSize.X = 0;
                personSize.Y = 80;
                CurrentLock = 0;
                CurrentlyOnScreen += AppearRate;
                active = false;
            }
        }

        public override void Animation(int speed)
        {
            PersonAnimate = new Rectangle(TextureWidth * CurrentFrame, 0, TextureWidth, TextureHeight);
            Distance = (Getlocation.X - personSize.X);

            if (Distance <= 60 && Distance > 0)
            {
                active = true;
            }

            if (personSize.Y > 700)
            {
                active = false;
            }
            CurrentFrame = (active ? 1 : 0);
        }

        public override void Movement(int Speed)
        {
            if (active)
            {
                personSize.Y += Speed;
            }
            else
            {
                personSize.X += (Speed-2);
            }
        }
    }

    class Torpedo : WatchingPlayer
    {
        private SoundEffect SFXenemy;
        public Torpedo(Texture2D image, int newSpeed, int newPoints, int maxAnim, int newunlock, int newLock, int maxappear) 
        :base(image, newSpeed, newPoints, maxAnim, newunlock, newLock, maxappear)
        {
            Speed = -22;
            personSize = new Rectangle(200, 0, (TextureWidth / 2), ((TextureHeight / 2)-20));
            SFXenemy = Content.Load<SoundEffect>("aani5");
        }

        public override void ResetStatus()
        {
            personSize.X = 800;
            CurrentLock = LockFromScreen;
        }

        public override void PlayerFound(int playerLocationX, int playerLocationY) 
        {
            if (playerLocationY < 80 && CurrentLock > 0)
            {
                SFXenemy.Play();
                CurrentLock = 0;
                personSize.X = 800;
                personSize.Y = 60;
                ReturnScore = true;
            }
        }

        public override void OutFromScreen(GraphicsDevice graphics)
        {
            if (personSize.X < -250 || isDamageTaken)
            {
                ResetStatus();
            }
        }
    }  

    class HidingGhost : WatchingPlayer
    {
        bool isGhostVisible;
        private float Distance;
        public HidingGhost(Texture2D image, int newSpeed, int newPoints, int maxAnim, int newunlock, int newLock, int maxappear) 
        :base(image, newSpeed, newPoints, maxAnim, newunlock, newLock, maxappear)
        {
        }

        public override void Animation(int Speed)
        {
            PersonAnimate = new Rectangle(TextureWidth * CurrentFrame, 0, TextureWidth, TextureHeight);
            CurrentFrame = 0;
            Distance = (Getlocation.X - personSize.X);

            if (personSize.X < 100 || Distance <= 50)
            {
                isGhostVisible = true;
            }
            else if (personSize.X > 150)
            {
                isGhostVisible = false;
            }
            else
            {
                if ((personSize.X % 2) == 0)
                {
                    isGhostVisible = !isGhostVisible;
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (isGhostVisible)
            {
                spriteBatch.Draw(paradePerson, personSize, PersonAnimate, Color.White);
            }
        }
    }
}