﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Linq;

namespace ParaatiOpen
{
    class ScrollingTexture
    {
        protected Texture2D Ground, BackgroundIMG, BackgroundIMG2, Line1, Line2;
        protected Texture2D[] Background = new Texture2D[5];
        protected Vector2 Offset;                                 
        protected Viewport Viewport;                              
        protected Rectangle Background1Size, Background2Size;
        protected int chosenBackground1, chosenBackground2, addSpeed, ScreenCenter;
        protected Random SelectBackground = new Random();
        protected Color[] Lines;

        private static ContentManager content;
        public static ContentManager Content
        {
            protected get { return content; }
            set { content = value; }
        }

        public ScrollingTexture(GraphicsDevice device) //Setup
        {
            Viewport = device.Viewport;
            Ground = Content.Load<Texture2D>("parallax");
            Offset = Vector2.Zero;

            for (int i = 0; i < 5; i++)
            {
                Background[i] = Content.Load<Texture2D>("tausta" + i);
            }

            ScreenCenter = (Viewport.Height / 2) +50;
            Line1 = new Texture2D(device, 1, 10);
            Line2 = new Texture2D(device, 1, 10);

            BackgroundIMG = Background[0];
            BackgroundIMG2 = Background[1];
            Background1Size = new Rectangle(0, -50, Viewport.Width, 300);
            Background2Size = new Rectangle((Background2Size.X-Viewport.Width) , -50, Viewport.Width, 300);

            Lines = new Color[1 * 10];
            for (int paint = 0; paint < Lines.Count(); paint++) // Create road surface marking lines
            {
                Lines[paint] = new Color(255,255,255);
            }
            Line1.SetData(Lines);
            Line2.SetData(Lines);
        }

        public void Update(int GameSpeed)
        {
            addSpeed = (GameSpeed);
            Background1Size.X += addSpeed;
            Background2Size.X += addSpeed;                     
            Offset.X += (-1 * addSpeed);                                    

            if (Offset.X < -Ground.Width)
            {
                Offset = new Vector2(-4,0);
            }

            if (Background1Size.X > Viewport.Width) // If background is out of game screen, change it
            {
                chosenBackground1 = SelectBackground.Next(0, 3); //0,2
                BackgroundIMG = Background[chosenBackground1];
                Background1Size.X = (Background2Size.X - Viewport.Width);
            }
            if (Background2Size.X > Viewport.Width)
            {
                chosenBackground2 = SelectBackground.Next((chosenBackground1 += 1),5);
                BackgroundIMG2 = Background[chosenBackground2];
                Background2Size.X = (Background1Size.X - Viewport.Width);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Ground, new Vector2(Viewport.X, ScreenCenter), new Rectangle((int)Offset.X, (int)Offset.Y, Viewport.Width, Ground.Height), Color.White);
            spriteBatch.Draw(BackgroundIMG, Background1Size, Color.White);
            spriteBatch.Draw(BackgroundIMG2, Background2Size, Color.White);
            spriteBatch.Draw(Line1, new Vector2(Viewport.X, BackgroundIMG.Height + 40), new Rectangle((int)Offset.X, (int)Offset.Y, Viewport.Width, Line1.Height), Color.White);
            spriteBatch.Draw(Line2, new Vector2(Viewport.X, Viewport.Height - 30), new Rectangle((int)Offset.X, (int)Offset.Y, Viewport.Width, Line2.Height), Color.White);
        }
    }
}