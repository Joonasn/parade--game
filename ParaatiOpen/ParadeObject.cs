﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace ParaatiOpen
{
    class ParadeObject // Base class
    {
        private Texture2D[] image = new Texture2D[8];
        private Rectangle[] Collision = new Rectangle[8];
        protected Texture2D paradePerson;
        public Rectangle personSize;
        protected Rectangle PersonAnimate;
        protected int Speed, Points, CurrentFrame, MaxFrames, TextureWidth, TextureHeight;
        protected int frameCycle, Framedalay = 4, addSpeed = 0, LastFrame = 0, newLocation = 200, AliveTimer=0;
        public bool AllowRemove = false, ReturnScore = false;
        protected Random Unlock = new Random();

        public ParadeObject(Texture2D image, int newSpeed, int newPoints, int maxAnim)
        {
            paradePerson = image;
            TextureWidth = (image.Width / maxAnim);
            TextureHeight = image.Height;
            personSize = new Rectangle(-100, 0, (TextureWidth / 3), (TextureHeight / 3));
            Speed = newSpeed;
            Points = newPoints;
            MaxFrames = maxAnim;
        }

        public virtual void Update(int GameSpeed, GraphicsDevice graphics) // Default object behaviour
        {
            addSpeed = (Speed + GameSpeed);
            Animation(addSpeed);
            Movement(addSpeed);
            OutFromScreen(graphics);
        }

        public virtual void Movement(int CurrentSpeed) // Default movement 
        {
            personSize.X += CurrentSpeed;
        }

        public virtual void Animation(int Speed) // Default animation 
        {
            PersonAnimate = new Rectangle(TextureWidth * CurrentFrame, 0, TextureWidth, TextureHeight);
            frameCycle++;
            if (frameCycle >= Framedalay)
            {
                CurrentFrame = (CurrentFrame + 1) % (MaxFrames - LastFrame);
                frameCycle = 0;
            }
        }

        public virtual void OutFromScreen(GraphicsDevice graphics) // If out from screen/ damage teken
        {
            if (personSize.X > graphics.Viewport.Width || personSize.X < -260 || AliveTimer > 20)
            {
                ReturnScore = true;
            }
        }

        public virtual int returnPoints() // Returns "Points" value to main class and removes object
        {
            AllowRemove = true;
            return Points;
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(paradePerson, personSize, PersonAnimate, Color.White);
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    class Items : ParadeObject  //Normal items
    {
        public int ItemValue;
        public Items(Texture2D image, int newSpeed, int newPoints, int maxAnim) // Setup
        : base(image, newSpeed, newPoints, maxAnim)
        {
            personSize = new Rectangle(-100, 0, (TextureWidth / 2), (TextureHeight / 2));
            Framedalay = 50;
        }

        public virtual void AddCopy(List<Items> ItemClones, Items ItemClone, int Y, int X, int ID) //Create copy
        {
            var itemClone = ItemClone.Clone() as Items;
            itemClone.personSize.Y = Y;
            itemClone.personSize.X = X;
            itemClone.ItemValue = ID;
            ItemClones.Add(itemClone);
        }
    }
    
    class PlayersWand : Items // a Wand item used by player
    {
        SoundEffect SFXenemy;
        public PlayersWand(Texture2D image, int newSpeed, int newPoints, int maxAnim, SoundEffect Sound)
        : base(image, newSpeed, newPoints, maxAnim)
        {
            Framedalay = 3;
            SFXenemy = Sound;
        }

        public virtual void PlaySFX()
        {
            SFXenemy.Play();
        } 

        public override void Movement(int CurrentSpeed)
        {
            personSize.X -= 15;
        }

        public override void OutFromScreen(GraphicsDevice graphics)
        {
            if (personSize.X > graphics.Viewport.Width || personSize.X < -260 || AliveTimer > 20)
            {
                AllowRemove = true;
            }
        }

        public virtual void AddWandCopy(List<PlayersWand> ItemClones, PlayersWand ItemClone, int Y, int X)
        {
            var itemClone = ItemClone.Clone() as PlayersWand;
            itemClone.personSize.Y = Y;
            itemClone.personSize.X = X;
            ItemClones.Add(itemClone);
        }
    }

    class ParadeFolk : ParadeObject // Regular Enemies, no player needed for act.
    {
        public bool isDamageTaken = false;
        public Rectangle personCollision;
        public ParadeFolk(Texture2D image, int newSpeed, int newPoints, int maxAnim)
        : base(image, newSpeed, newPoints, maxAnim)
        {
            personCollision = new Rectangle(personSize.X, personSize.Y, ((personSize.Width / 2) + 30), ((personSize.Height / 2) + 30));
            LastFrame = 1;
            AliveTimer = 0;
        }

        public virtual void Update(int GameSpeed, GraphicsDevice graphics, List<ParadeFolk> ParadeClones, List<Items> ItemClones, int X, int Y)
        {
            addSpeed = (Speed + GameSpeed);
            personCollision.X = (personSize.X + ((personSize.Width / 3) - 10));
            personCollision.Y = (personSize.Y + (personSize.Height / 3));

            if (isDamageTaken)
            {
                CurrentFrame = (MaxFrames - LastFrame);
                AliveTimer++;
            }
            else
            {
                Movement(addSpeed);
            }
            Animation(addSpeed);
            UpdateItemDrop(ParadeClones);
            OutFromScreen(graphics);
            PlayerFound(X, Y);
        }

        public virtual void UpdateItemDrop(List<ParadeFolk> ParadeClones)
        {

        }

        public virtual void PlayerFound(int playerLocationX, int playerLocationY)
        {

        }

        public virtual void AddCopy(List<ParadeFolk> ParadeClones, ParadeFolk PersonClone)
        {
            var paradeperson = PersonClone.Clone() as ParadeFolk;
            paradeperson.newLocation = Unlock.Next(200, 600);
            paradeperson.personSize.Y = paradeperson.newLocation;
            ParadeClones.Add(paradeperson);
        }
    }

    class RowFolk : ParadeFolk
    {
        protected int getLocation=200;
        public RowFolk(Texture2D image, int newSpeed, int newPoints, int maxAnim)
        : base(image, newSpeed, newPoints, maxAnim)
        {
            
        }

        public override void AddCopy(List<ParadeFolk> ParadeClones, ParadeFolk PersonClone)
        {
            int getLocation = Unlock.Next(200, 600);
            for (int i=0; i <3; i++)
            {
                var paradeperson = PersonClone.Clone() as RowFolk;
                paradeperson.personSize.Y = (getLocation + (i*50));
                ParadeClones.Add(paradeperson);
            }

        }
    }

    class FloatyGhost : ParadeFolk
    {
        public FloatyGhost(Texture2D image, int newSpeed, int newPoints, int maxAnim)
        : base(image, newSpeed, newPoints, maxAnim)
        {
        }

        public override void Animation(int Speed)
        {
            PersonAnimate = new Rectangle(TextureWidth * CurrentFrame, 0, TextureWidth, TextureHeight);
            CurrentFrame = 1;
        }
        public override void Movement(int CurrentSpeed)
        {
            personSize.X += CurrentSpeed;
            personSize.Y += (int)(10 * Math.Sin((personSize.X * 0.2) * 0.5 * 2 / 20));
        }
    }

    class Squirrel : ParadeFolk // Enemies that Drop items 
    {
        protected int Waiting, ChangeAnimation=900;
        public Wand SquirrelsWand;
        public Squirrel(Texture2D image, int newSpeed, int newPoints, int maxAnim)
        : base(image, newSpeed, newPoints, maxAnim)
        {
            Waiting = 0;
        }

        public override void Movement(int CurrentSpeed)
        {
            if (personSize.X >= ChangeAnimation)
            {
                personSize.X += 3;
                Waiting++;

                if (Waiting >= 60)
                {
                    Waiting = 61;
                }
            }
            else
            {
                personSize.X += CurrentSpeed;
            }
        }

        public override void Animation(int AddSpeed)
        {
            PersonAnimate = new Rectangle(TextureWidth * CurrentFrame, 0, TextureWidth, TextureHeight);
            if (isDamageTaken)
            {
                CurrentFrame = MaxFrames;
            }
            else
            {
                if (personSize.X >= ChangeAnimation)
                {
                    if (Waiting >= 60) //Wand is not returning, play different animation
                    {
                        CurrentFrame = 8;
                    }
                    else
                    {
                        CurrentFrame = 7;
                    }
                }
                else
                {
                    frameCycle++;
                    if (frameCycle >= Framedalay)
                    {
                        CurrentFrame = (CurrentFrame + 1) % (MaxFrames - 3);
                        frameCycle = 0;
                    }
                }
            }
        }

        public override void UpdateItemDrop(List<ParadeFolk> ParadeClones)
        {
            if (Waiting == 2)
            {
                AddCopyItem(ParadeClones);
                Waiting = 3;
            }
        }

        public virtual void AddCopyItem(List<ParadeFolk> ParadeClones)
        {
            var squirrelsWand = SquirrelsWand.Clone() as Wand;
            squirrelsWand.personSize.X = (this.personSize.X -10);
            squirrelsWand.personSize.Y = (this.personSize.Y -20);
            ParadeClones.Add(squirrelsWand);
            squirrelsWand.Setup();
        }
    }

    class Wand : ParadeFolk
    {
        public Items WandCollect;
        protected int DirectionThrow = 0, DirectionThrowMax = -30, SpeedValue = -2, Length;
        public Wand(Texture2D image, int newSpeed, int newPoints, int maxAnim)
        : base(image, newSpeed, newPoints, maxAnim)
        {
            personSize = new Rectangle(0, 0, (TextureWidth / 2), (TextureHeight / 2));
            Framedalay = 7;
            LastFrame = 0;
        }

        public virtual void Setup()
        {
            Length = Unlock.Next(0, 5);
            if (Length > 2)
            {
                DirectionThrowMax = -200;
            }
        }

        public override void Update(int GameSpeed, GraphicsDevice graphics, List<ParadeFolk> ParadeClones, List<Items> ItemClones, int X, int Y)
        {
            personCollision.X = personSize.X;
            personCollision.Y = personSize.Y;
            if (DirectionThrow < DirectionThrowMax)
            {
                SpeedValue = 2;
            }

            DirectionThrow += SpeedValue;
            Movement(DirectionThrow/2);
            Animation(GameSpeed);
            OutFromScreen(graphics, ItemClones);
        }

        public virtual void OutFromScreen(GraphicsDevice graphics, List<Items> ItemClones)
        {
            if (personSize.X > graphics.Viewport.Width)
            {
                ReturnScore = true;
            }
            else if (personSize.X < -150)
            {
                AllowRemove = true;
                WandCollect.AddCopy(ItemClones, WandCollect, personSize.Y, -100, 3);
            }
        }
    }

    class TubaPlayer : ParadeFolk
    {
        public Items TubaInstru;
        public TubaPlayer(Texture2D image, int newSpeed, int newPoints, int maxAnim)
        : base(image, newSpeed, newPoints, maxAnim)
        {
            
        }

        public override void Update(int GameSpeed, GraphicsDevice graphics, List<ParadeFolk> ParadeClones, List<Items> ItemClones, int X, int Y)
        {
            addSpeed = (Speed + GameSpeed);
            personCollision.X = (personSize.X + ((personSize.Width / 3) - 10));
            personCollision.Y = (personSize.Y + (personSize.Height / 3));

            if (isDamageTaken)
            {
                CurrentFrame = (MaxFrames - LastFrame);
                AliveTimer++;

                if (AliveTimer == 15)
                {
                    TubaInstru.AddCopy(ItemClones, TubaInstru, this.personSize.Y, this.personSize.X + 10, 4);
                }
            }
            else
            {
                Movement(addSpeed);
            }
            Animation(addSpeed);
            OutFromScreen(graphics);
        }
    }

    class WatchingPlayer : ParadeFolk // Classes that need players position to act.
    {
        protected bool isActive;
        protected float Distance;
        protected Vector2 Getlocation;
        public WatchingPlayer(Texture2D image, int newSpeed, int newPoints, int maxAnim)
        : base(image, newSpeed, newPoints, maxAnim)
        {
            personSize = new Rectangle(-100, 0, ((TextureWidth / 2) - 10), (TextureHeight / 2) -10);
        }

        public override void PlayerFound(int playerLocationX, int playerLocationY) //Method for Torpedo, Wand and HidingGhost enemies
        {
            Getlocation.X = playerLocationX;
            Getlocation.X = playerLocationY;
        }
    }

    class Agent : WatchingPlayer
    {
        public Agent(Texture2D image, int newSpeed, int newPoints, int maxAnim)
        : base(image, newSpeed, newPoints, maxAnim)
        {
            isActive = false;
        }

        public override void Animation(int speed)
        {
            PersonAnimate = new Rectangle(TextureWidth * CurrentFrame, 0, TextureWidth, TextureHeight);
            Distance = (Getlocation.X - personSize.X);

            if (Distance <= 60 && Distance > 0)
            {
                isActive = true;
            }

            if (personSize.Y > 600)
            {
                isActive = false;
            }
            CurrentFrame = (isActive ? 1 : 0);
        }

        public override void Movement(int Speed)
        {
            if (isActive)
            {
                personSize.Y += Speed;
            }
            else
            {
                personSize.X += (Speed - 2);
            }
        }

        public override void AddCopy(List<ParadeFolk> ParadeClones, ParadeFolk PersonClone)
        {
            var paradeperson = PersonClone.Clone() as Agent;
            paradeperson.personSize.Y = 20;
            ParadeClones.Add(paradeperson);
        }
    }

    class Torpedo : WatchingPlayer
    {
        private int CurrentLock, CustomSpeed;
        SpriteEffects mirror;
        private SoundEffect SFXenemy;
        public Torpedo(Texture2D image, int newSpeed, int newPoints, int maxAnim, SoundEffect Sound, GraphicsDevice graphics)
        : base(image, newSpeed, newPoints, maxAnim)
        {
            personSize = new Rectangle(graphics.Viewport.Width, 40, (TextureWidth / 2), ((TextureHeight / 2) - 20));
            CurrentLock = 1;
            CustomSpeed = -1;
            SFXenemy = Sound;
            mirror = SpriteEffects.None;
        }

        public override void PlayerFound(int playerLocationX, int playerLocationY)
        {
            if (playerLocationY < 80 && CurrentLock > 0)
            {
                SFXenemy.Play();
                personSize.Y = 40;
                CurrentLock = 0;
                AliveTimer = 0;
            }
        }

        public override void Update(int GameSpeed, GraphicsDevice graphics)
        {
            addSpeed = (Speed + (GameSpeed*2));
            personCollision.X = (personSize.X + ((personSize.Width / 3) - 10));
            personCollision.Y = (personSize.Y + (personSize.Height / 3));

            if (CurrentLock == 0)
            {
                if (isDamageTaken)
                {
                    CurrentFrame = (MaxFrames - LastFrame);
                    AliveTimer++;
                }
                else
                {
                    Movement(addSpeed);
                }
                Animation(addSpeed);
            }
            OutFromScreen(graphics);
        }

        public override void Movement(int CurrentSpeed)
        {
            personSize.X += (CurrentSpeed * CustomSpeed);
        }

        public override void OutFromScreen(GraphicsDevice graphics)
        {
            if (personSize.X < -300)
            {
                personSize.X = -290;
                mirror = SpriteEffects.FlipHorizontally;
                CustomSpeed = 1;
                CurrentLock = 1;
                ReturnScore = true;
            }
            else if (personSize.X > graphics.Viewport.Width + 50)
            {
                personSize.X = graphics.Viewport.Width;
                mirror = SpriteEffects.None;
                CustomSpeed = -1;
                CurrentLock = 1;
                ReturnScore = true;
            }
        }

        public override int returnPoints()
        {
            ReturnScore = false;
            return Points;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(paradePerson, personSize, PersonAnimate, Color.White, 0.0f, Vector2.Zero, mirror, 0.0f);
        }
    }

    class HidingGhost : WatchingPlayer
    {
        
        public HidingGhost(Texture2D image, int newSpeed, int newPoints, int maxAnim)
        : base(image, newSpeed, newPoints, maxAnim)
        {
        }

        public override void Animation(int Speed)
        {
            PersonAnimate = new Rectangle(TextureWidth * CurrentFrame, 0, TextureWidth, TextureHeight);
            CurrentFrame = 0;
            Distance = (Getlocation.X - personSize.X);

            if (personSize.X < 100 || Distance <=50 || isDamageTaken)
            {
                isActive = true;
            }
            else if (personSize.X > 150)
            {
                isActive = false;
            }
            else
            {
                if ((personSize.X % 2) == 0)
                {
                    isActive = !isActive;
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (isActive)
            {
                spriteBatch.Draw(paradePerson, personSize, PersonAnimate, Color.White);
            }
        }
    }
}