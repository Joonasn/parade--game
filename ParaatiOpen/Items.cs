﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace ParaatiOpen
{
    class Items
    {
        protected Random itemSpawn = new Random();
        protected Texture2D[] item = new Texture2D[5];
        public Rectangle[] itemCollision = new Rectangle[5];
        protected Rectangle[] itemAnimation = new Rectangle[5];
        public bool[] itemActive = new bool[5];
        protected float itemTimer = 0, delay = 200;
        protected int PositionX = -200, itemFrame=0, numberOfFrames=2, addSpeed;

        private static ContentManager content;
        public static ContentManager Content
        {
            protected get { return content; }
            set { content = value; }
        }

        public Items ()
        {
            for (int i = 0; i < item.Length; i++)
            {
                item[i] = Content.Load<Texture2D>("tavara" + i);
                itemCollision[i] = new Rectangle(-200, -500, 100, 100);
                itemActive[i] = false;
            }
        }

        public void TurnItemActive(int item, int ItemStartPositionY, int ItemStartPositionX)
        {
            if (ItemStartPositionY <= 0)
            {
                itemCollision[item].Y = itemSpawn.Next(180, 700);
                itemCollision[item].X = PositionX;
            }
            else
            {
                itemCollision[item].Y = ItemStartPositionY;
                itemCollision[item].X = ItemStartPositionX;
            }
            itemActive[item] = true;
        }

        public void ResetItem (int i)
        {
            itemCollision[i].X = PositionX;
            itemActive[i] = false;
        }

        public void Update (GraphicsDevice graphics, GameTime gameTime, int parallaxSpeed, int GameSpeed)
        {
            itemTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            addSpeed = (parallaxSpeed + GameSpeed);

            for (int i = 0; i < item.Length; i++) // Animation
            {
                itemAnimation[i] = new Rectangle((item[i].Width / 2) * itemFrame, 0, (item[i].Width / 2), item[i].Height);
            }

            if (itemTimer >= delay) // Animation, frame delay 
            {
                itemFrame = (itemFrame + 1) % numberOfFrames;
                itemTimer = 0;
            }

            for (int i = 0; i < itemCollision.Length; i++)
            {
                if (itemActive[i]) // If item is Active
                {
                    itemCollision[i].X += addSpeed;
                }

                if (itemCollision[i].X > graphics.Viewport.Width) // Out of play area; Return to start position 
                {
                    ResetItem(i);
                }
            }
        }

        public void Draw (SpriteBatch spriteBatch)
        {
            for (int i = 0; i < item.Length; i++)
            {
                spriteBatch.Draw(item[i], itemCollision[i], itemAnimation[i], Color.White);
            }
        }
    }
}