﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace ParaatiOpen
{
    class Player
    {
        Texture2D[] PlayerTexture = new Texture2D[2];             
        public Rectangle PlayerSize, PlayerAnimation, PlayerCollision;
        public string[] HoldingText = new string[] { "", "Cloak","Clock", "Wand", "Tuba" };
        public bool AllowScore = false, usingWand = false, ThrowWand = false, TubaActive = false, OutFromScreen = false, CoinCollected = false, ClockUsed=false;
        public int invisibleCount=0, CurrentHolding = 0;
        private int CurrentTexture=0, playerFrame = 0, playerSpeed=4, FrameCount=0, invisibleCountMAX=250;
        SpriteEffects mirror;
        Keys[] KeysMovement = new[] { Keys.Up, Keys.W, Keys.Left, Keys.A, Keys.Down, Keys.S, Keys.Right, Keys.D };
        private List<SoundEffect> SFX = new List<SoundEffect>();

        private static ContentManager content;
        public static ContentManager Content
        {
            protected get { return content; }
            set { content = value; }
        }

        public Player (GraphicsDevice graphics) // Setup
        {
            PlayerTexture[0] = Content.Load<Texture2D>("pelaajawalk1");
            PlayerTexture[1] = Content.Load<Texture2D>("pelaajawalk2");
            PlayerSize = new Rectangle(graphics.Viewport.Width, 300, 145, 230);
            PlayerCollision = new Rectangle(PlayerSize.X, PlayerSize.Y, ((PlayerSize.Width / 3) + 30), ((PlayerSize.Height / 3) + 30));

            for (int i= 0; i < 4; i++)
            {
                SFX.Add(Content.Load<SoundEffect>("aani"+i));
            }
        }

        public void ResetPlayer(GraphicsDevice graphics) // Reset player before starting the game
        {
            CurrentHolding = 0;
            invisibleCount = invisibleCountMAX;
            OutFromScreen = false;
            mirror = SpriteEffects.None;

            if (PlayerSize.X > (graphics.Viewport.Width) || PlayerSize.X < -100)
            {
                PlayerSize.Y = (graphics.Viewport.Height / 2);
                PlayerSize.X = graphics.Viewport.Width;
            }
        }

        protected bool IfKeyDown(KeyboardState state, params Keys[] KeysMovement) // Check If selected key(s) are pressed
        {
            bool result = false;
            for (int i = 0; i < KeysMovement.Length; i++)
            {
                result |= state.IsKeyDown(KeysMovement[i]);
            }
            return result;
        }

        protected bool IfKeyReleased(KeyboardState state) // Check if key is released
        {
            bool result = false;
            for (int i = 0; i < KeysMovement.Length; i++)
            {
                result = state.IsKeyUp(KeysMovement[i]);
                if (!result) // Ends loop and returns true if result is false
                {
                    break;
                }
            }
            return result;
        }

        public void UpdateMovement(KeyboardState state, int SpeedBonus, GraphicsDevice graphics) // Keyboard controls
        {
            int addSpeed = (playerSpeed + SpeedBonus);
            PlayerCollision.X = (PlayerSize.X + ((PlayerSize.Width / 3) - 10));
            PlayerCollision.Y = (PlayerSize.Y + (PlayerSize.Height / 3) - 20);

            if (IfKeyDown(state, Keys.Right, Keys.D)) // Right
            {
                PlayerSize.X += addSpeed;
                mirror = SpriteEffects.FlipHorizontally;
            }
            else if (IfKeyDown(state, Keys.Left, Keys.A)) // Left
            {
                PlayerSize.X -= addSpeed;
                mirror = SpriteEffects.None;
            }
            else
            {
                PlayerSize.X += SpeedBonus; // Gradually move to right
            }

            if (IfKeyDown(state, Keys.Up, Keys.W) && PlayerSize.Y > 0) // Up
            {
                PlayerSize.Y -= addSpeed;
            }
            else if (IfKeyDown(state, Keys.Down, Keys.S) && PlayerSize.Y < (graphics.Viewport.Height-100)) // Down
            {
                PlayerSize.Y += addSpeed;
            }

            if (state.IsKeyDown(Keys.Space) && CurrentHolding != 0) // Use items 
            {
                ReleaseItem();
            }
            AllowScore = !IfKeyReleased(state);
            CorrectAnimation(state, graphics);
            NonKeyActions(graphics);
        }

        public void NonKeyActions(GraphicsDevice graphics) // Automatic actions
        {
            if (invisibleCount > 0) // Player is damaged/ using cloak. 
            {
                invisibleCount--;
            }

            if (PlayerSize.X > graphics.Viewport.Width || PlayerSize.X < -180) // Outside of in game screen means game over
            {
                OutFromScreen = true;
            }
        }

        public void EnemyCollision() // Collision with enemy
        {
            if (CoinCollected) // If the player has a coin; takes damage
            {
                CoinCollected = false;
                invisibleCount = 250;
            }
            else // otherwise; game over!
            {
                OutFromScreen = true;
            }
            SFX[1].Play();
        }

        public void ItemCollision(int itemValue) // Collision wth item
        {
            if (itemValue==0 || itemValue < 0) //Coin
            {
                CoinCollected = true;
            }
            else
            {
                if (itemValue > HoldingText.Length) 
                {
                    itemValue = 1;
                }
                else
                {
                    CurrentHolding = itemValue;
                }
            }
            SFX[2].Play();
        }

        protected void ReleaseItem() // Use item
        {
            switch (CurrentHolding)
            {
                case 1: // Cloak
                    invisibleCount = invisibleCountMAX;  
                    break;
                case 3: // Wand
                    if (!usingWand)  
                    {
                        SFX[0].Play();
                        FrameCount = 0;
                        playerFrame = 5;
                        usingWand = true;
                    }
                    break;
                case 4: // Tuba
                    SFX[3].Play(); 
                    TubaActive = true;
                    break;
                default:  // Clock
                    ClockUsed = true;
                    break;
            }
            CurrentHolding = 0; // No item
        }

        protected void CorrectAnimation (KeyboardState state, GraphicsDevice graphics) // Animation 
        { 
            if (usingWand) // If player was holding a "wand" -item
            {
                if (playerFrame >= 7)
                {
                    usingWand = false;
                    ThrowWand = true;
                }
                Animation(1, 6, 8, false);
                mirror = SpriteEffects.None;
            }
            else
            {
                if (!IfKeyReleased(state)) // Check if keys pressed; Keys pressed
                {
                    Animation(0, 4, 8, true);
                }
                else // No Key(s) pressed
                {
                    if (playerFrame >1)
                    {
                        playerFrame = 0;
                    }
                    Animation(1, 20, 2, true);
                    mirror = SpriteEffects.None;
                }
            }
        }

        public void IntroWalking(int SpeedBonus) // Executed when new game starts (but only if player position(X) < Screen center)
        {
            Animation(0, 3, 8, true);
            PlayerSize.X -= (playerSpeed + SpeedBonus);
        }

        public void GameOverAnimation(bool frame) //Executed when player gets "game over"
        {
            playerFrame = 2 + (frame ? 1 : 0);
            Animation(1, 30, 8, false);
        }

        protected void Animation(int Texture, int FrameCountMAX, int FrameDivider, bool IsLooping) // Handle players animation
        {
            CurrentTexture = Texture;
            if (FrameCount > FrameCountMAX || FrameCount < 0)
            {
                playerFrame = (playerFrame + 1) % FrameDivider;
                if (IsLooping)
                {
                    FrameCount = 0;
                }
            }
            else
            {
                FrameCount++; //adjust animation timer
            }
            PlayerAnimation = new Rectangle((500 * playerFrame), 0, 500, 750);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (invisibleCount % 2 == 0)
            {
                spriteBatch.Draw(PlayerTexture[CurrentTexture], PlayerSize, PlayerAnimation, Color.White, 0.0f, Vector2.Zero, mirror, 0.0f); 
            }
        }
    }
}