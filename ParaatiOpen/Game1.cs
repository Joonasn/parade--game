﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Microsoft.Xna.Framework.Media;

namespace ParaatiOpen
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch, SBParallax;
        SpriteFont Font;
        KeyboardState state, pauseState, AudioState;
        Song inGameTune;
        SoundEffect enemySFX1, enemySFX2;
        TitleScreen Title;
        ScrollingTexture Scroll;
        HUDScore HUD;
        Player Character;
        List<ParadeFolk> ParadeList = new List<ParadeFolk>();
        List<ParadeFolk> ParadeClones = new List<ParadeFolk>();
        List<Items> Items = new List<Items>();
        List<Items> ItemClones = new List<Items>();
        List<PlayersWand> ThrowWandClones = new List<PlayersWand>();
        Torpedo BikerGuy;
        PlayersWand WandTemplate;
        Texture2D[] ParadeImage = new Texture2D[8];
        Texture2D[] ItemImage = new Texture2D[5];
        XmlDocument SaveFile = new XmlDocument();
        Random EnemySelector = new Random();
        Random ItemSelector = new Random();

        const int ScreenWidth = 1200, ScreenHeight = 800; // Adjust resolution
        int GameSpeed=5, CurrentLevelMax, CurrentLevel=1, CurrentLevelReset=1, SpeedRise, SpeedRiseDelay = 40, 
            Score = 0, HiScore = 7000, WalkingScoreDelay = 0, resetTimer = 0;
        bool gameover = false, pause = false, AcceptExit = false, GameMode = false, intro = false, ShowHighScore = false, audioSetup = true;
        float ReleaseTimer = 0, ReleaseDelay = 1000f;
        int[] EnemyWeight, WeightStack;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = ScreenWidth;
            graphics.PreferredBackBufferHeight = ScreenHeight;
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            TitleScreen.Content = Content;
            ScrollingTexture.Content = Content;
            HUDScore.Content = Content;
            Player.Content = Content;
            MediaPlayer.IsRepeating = true;

            base.Initialize();

            if (File.Exists("SharpDXV.xml"))
            {
                SaveFile.Load("SharpDXV.xml");
                Score = Convert.ToInt32(SaveFile.SelectSingleNode("Pisteet/Nykyinen").InnerText);
                HiScore = Convert.ToInt32(SaveFile.SelectSingleNode("Pisteet/Huipputulos").InnerText);
            }
            else
            {
                Score = 0;
                HiScore = 10000;
            }
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            SBParallax = new SpriteBatch(GraphicsDevice);
            Font = Content.Load<SpriteFont>("fontti");
            Title = new TitleScreen(Font, ScreenWidth, ScreenHeight);
            HUD = new HUDScore(Font, ScreenWidth, ScreenHeight);
            Scroll = new ScrollingTexture(GraphicsDevice);
            inGameTune = Content.Load<Song>("musa");
            Character = new Player(GraphicsDevice);
            enemySFX1 = Content.Load<SoundEffect>("aani4");
            enemySFX2 = Content.Load<SoundEffect>("aani5");

            for (int i = 0; i < ParadeImage.Length; i++)
            {
                ParadeImage[i] = Content.Load<Texture2D>("enemy" + i);
            }

            for (int i = 0; i < ItemImage.Length; i++)
            {
                ItemImage[i] = Content.Load<Texture2D>("tavara" + i);
                Items.Add(new Items(ItemImage[i], 1, 0, 2));
            }

            ParadeList.Add(new ParadeFolk(ParadeImage[0], 3, 50, 6)); // Normal enemy (0) Weight = 6
            ParadeList.Add(new RowFolk(ParadeImage[1], 3, 50, 6)); // "Row" enemy (1) Weight = 5

            ParadeList.Add(
            new TubaPlayer(ParadeImage[3], 1, 300, 7) // Tuba enemy (2) Weight = 4
            {
                TubaInstru = new Items(ItemImage[4], 1, 0, 2), ////// Collectable Tuba instrument
            });
            ParadeList.Add(
            new Squirrel(ParadeImage[2], 4, 50, 10) // Squirrel (3) Weight = 3
            {
                SquirrelsWand = new Wand(ParadeImage[7], 4, 0, 5) // Wand 
                {
                    WandCollect = new Items(ItemImage[3], 1, 0, 2), // Collectable Wand
                }
            });
   
            ParadeList.Add(new FloatyGhost(ParadeImage[6], 5, 50, 3)); // Floating Ghost (4) Weight = 2
            ParadeList.Add(new HidingGhost(ParadeImage[6], 3, 150, 3)); //Disappearing Ghost (5) Weight = 1
            ParadeList.Add(new Agent(ParadeImage[4], 2, 500, 3));  /////// Agent (6) Weight = 0
            BikerGuy = new Torpedo(ParadeImage[5], 5, 100, 3, enemySFX2, GraphicsDevice); /////// Torpedo
            WandTemplate = new PlayersWand(ParadeImage[7], 10, 200, 5, enemySFX1); // UsableWand

            EnemyWeight = new int [ParadeList.Count]; // Enemyweight = 7 (for "Weighted Random Selector")
            for (int i=0; i < ParadeList.Count; i++)
            {
                EnemyWeight[i] = ParadeList.Count - i;  // Normal = 7.. Row = 6 .. Tuba = 5 and so on...
            }
            WeightStack = new int [ParadeList.Count]; // WeightStack = 6 (Array)
            WeightStack[0] = EnemyWeight[0];   // Setup; WeightStack[0] = 7

            for (int i = 1; i < WeightStack.Length; i++) // Start counting from column 1 (Row enemy)
            {
                WeightStack[i] = WeightStack[i - 1] + EnemyWeight[i]; // WeightStack[1] = 7 + 6 (12) <-- Stack for Row enemy
            }
            CurrentLevelMax = (WeightStack.Length -1); // Since there's no column 7, maximun "CurrentLevel" value is 6
        }

        protected override void UnloadContent()
        {

        }

        protected void ResetValues() // Reset values when starting a new game
        {
            CurrentLevel = CurrentLevelReset;
            Score = 0;
            SpeedRise = 0;
            resetTimer = 0;
            gameover = false;
            pause = false;
            AcceptExit = false;
            ShowHighScore = false;
            pauseState = state;
            HUD.ResetHUD();
            Character.ResetPlayer(GraphicsDevice);
            ParadeClones.Clear();
            ItemClones.Clear();
        }

        protected void inGameReset() // If player uses clock, these values get a reset
        {
            CurrentLevel = CurrentLevelReset;
            SpeedRise = 0;
            resetTimer = 0;
            Character.ClockUsed = false;
        }

        protected void SaveScores() // Save current "Score" and "HiScore" values to XML -file
        {
            if (File.Exists("SharpDXV.xml")) // If file "SharpDXV.xml" is found, modify it
            {
                SaveFile.Load("SharpDXV.xml");
                XmlNodeList piste1 = SaveFile.SelectNodes("//Pisteet/Nykyinen");
                XmlNodeList piste2 = SaveFile.SelectNodes("//Pisteet/Huipputulos");

                foreach (XmlNode CurrentScoreNmb in piste1)
                {
                    CurrentScoreNmb.InnerText = Score.ToString();
                }
                foreach (XmlNode HiScoreNmb in piste2)
                {
                    HiScoreNmb.InnerText = HiScore.ToString();
                }
                SaveFile.Save("SharpDXV.xml");
            }
            else // If file "SharpDXV.xml" is missing create new one
            {
                XmlWriter xmlWriter = XmlWriter.Create("SharpDXV.xml");
                xmlWriter.WriteStartDocument();
                xmlWriter.WriteStartElement("Pisteet");
                xmlWriter.WriteStartElement("Nykyinen");
                xmlWriter.WriteString(Score.ToString());
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("Huipputulos");
                xmlWriter.WriteString(HiScore.ToString());
                xmlWriter.WriteEndDocument();
                xmlWriter.Close();
            }
        }

        protected void HandleGameSpeed(GameTime gameTime) // Actions related to gamespeed
        {
            ReleaseDelay = (700f -(float)(CurrentLevel*100));
            GameSpeed = (4 + CurrentLevel);

            ReleaseTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            if (ReleaseTimer > ReleaseDelay) // Release enemies 
            {
                SpeedRise++;
                if (SpeedRise == SpeedRiseDelay)
                {
                    UnlockItems();
                }
                ReleaseTimer = 0;
                UnlockParade();
            }

            if (SpeedRise > (SpeedRiseDelay + GameSpeed))
            {
                if (CurrentLevel < CurrentLevelMax)
                {
                    CurrentLevel++;
                }
                SpeedRise = 0;
            }

            if (Character.ClockUsed)
            {
                inGameReset();
            }

            if (CurrentLevel > CurrentLevelMax)
            {
                CurrentLevel = CurrentLevelMax;
                CurrentLevelReset = CurrentLevelMax;
            }
        }

        protected void HandleScore()
        {
            if (Score > HiScore)
            {
                if (!ShowHighScore)
                {
                    ShowHighScore = true;
                }
                HiScore = Score;
            }

            if (Score <= 9999999)
            {
                if (Character.AllowScore)
                {
                   WalkingScoreDelay++;
                }
                else
                {
                    WalkingScoreDelay = 0;
                }

                if (WalkingScoreDelay > (20 + GameSpeed))
                {
                    Score++;
                    WalkingScoreDelay = 0;
                }

                for (int i = 0; i < ParadeClones.Count; i++)
                {
                    if (ParadeClones[i].ReturnScore)
                    {
                        Score += ParadeClones[i].returnPoints();
                    }
                }

                for (int i = 0; i < ThrowWandClones.Count; i++)
                {
                    if (ThrowWandClones[i].ReturnScore)
                    {
                        Score += ThrowWandClones[i].returnPoints();
                    }
                }

                if (BikerGuy.ReturnScore)
                {
                    Score += BikerGuy.returnPoints();
                }
            }
            else
            {
                Score = 9999999;
            }
        }

        protected void HandleSubClasses() // Actions for subclasses, mostly from player
        {
            for (int i = 0; i < ItemClones.Count; i++)
            {
                if (Character.invisibleCount == 0 && Character.PlayerSize.Intersects(ItemClones[i].personSize)) // Collision with playable character IF CLOAK IS OFF
                {
                    Character.ItemCollision(ItemClones[i].ItemValue);
                    ItemClones[i].AllowRemove = true;
                }
            }

            if (Character.ThrowWand)  // Character is preparing to throw a wand
            {
                Character.ThrowWand = false;
                WandTemplate.AddWandCopy(ThrowWandClones, WandTemplate, Character.PlayerCollision.Y, Character.PlayerCollision.X - 30);
            }

            if (Character.TubaActive) // Character is preparing to play tuba
            {
                for (int i = 0; i < ParadeClones.Count; i++) // Onscreen enemies gets damaged
                {
                    ParadeClones[i].isDamageTaken = true;
                }
                Character.TubaActive = false;
            }

            for (int i = 0; i < ParadeClones.Count; i++) // Enemy Collision
            {
                if (Character.invisibleCount == 0 && Character.PlayerCollision.Intersects(ParadeClones[i].personCollision)) // Player and Parade collision
                {
                    if (!ParadeClones[i].isDamageTaken)
                    {
                        Character.EnemyCollision();
                    }
                }

                for (int j = 0; j < ThrowWandClones.Count; j++) // Parade and Wand Collision
                {
                    if (ParadeClones[i].personSize.Intersects(ThrowWandClones[j].personSize)) 
                    {
                        ParadeClones[i].isDamageTaken = true;
                        ThrowWandClones[j].PlaySFX();
                        ThrowWandClones[j].ReturnScore = true;
                    }
                }
            }

            if (Character.invisibleCount == 0 && Character.PlayerCollision.Intersects(BikerGuy.personCollision)) // Collision on Biker character
            {
                if (!BikerGuy.isDamageTaken)
                {
                    Character.EnemyCollision();
                }
            }
        }

        protected void UnlockParade() // Add new parade member to game screen
        {
            int WeightCheck = 0, NextEnemy=0;
            WeightCheck = EnemySelector.Next(0, WeightStack[CurrentLevel]) +1;
            NextEnemy = Array.BinarySearch(WeightStack, WeightCheck);

            if (NextEnemy > ParadeList.Count || NextEnemy < 0)
            {
                ParadeList[0].AddCopy(ParadeClones, ParadeList[0]);
            }
            else
            {
                ParadeList[NextEnemy].AddCopy(ParadeClones, ParadeList[NextEnemy]);
            }
        }

        protected void UnlockItems() // Add items to game screen
        {
            int NextItem = 0, SelectedItem = 0, nextPosition;
            NextItem = ItemSelector.Next(0, GameSpeed);

            if (NextItem % 2 == 0)
            {
                if (Score > 10000 && !Character.CoinCollected && CurrentLevel > 4)
                {
                    SelectedItem = 0; //Coin
                }
                else
                {
                    SelectedItem = 1; //Cloak
                }  
            }
            else
            {
                if (CurrentLevel > 2 && Character.CurrentHolding == 1) 
                {
                    SelectedItem = 2; //Clock
                }
                else
                {
                    SelectedItem = 1; //Cloak
                }
            }
            nextPosition = ItemSelector.Next(240, 650);

            if (Character.CurrentHolding != SelectedItem && SelectedItem <= Items.Count)
            {
                Items[SelectedItem].AddCopy(ItemClones, Items[SelectedItem], nextPosition, -100, SelectedItem);
            }
        }

        protected override void Update(GameTime gameTime)
        {
            state = Keyboard.GetState();
            if (state.IsKeyDown(Keys.Escape) && AcceptExit)
            {
                SaveScores();
                Exit();
            }

            if (!pause ^ gameover ^ intro) // Update Scrolling if Pause, Gameover and Intro values are false
            {
                Scroll.Update(GameSpeed);
            }

            if (gameover ^ pause | !GameMode) //Allow Exit and Full Screen -toggle, if Gameover or Pause values are true
            {
                AcceptExit = true;
                if (state.IsKeyDown(Keys.F))
                {
                    graphics.IsFullScreen = !graphics.IsFullScreen;
                    graphics.ApplyChanges();
                }
            }

            switch (GameMode)
            {
                case false: //Title screen
                    Title.Update(gameTime);
                    if (state.IsKeyDown(Keys.Enter))   
                    {
                        ResetValues();
                        intro = true;
                        GameMode = true;
                    }
                    break;
                case true: // Game mode
                default:
                    MainLoop(gameTime);
                    break;
            }
            base.Update(gameTime);
        }

        protected void MainLoop (GameTime gameTime)
        {
            if (state.IsKeyDown(Keys.Enter) && pauseState.IsKeyUp(Keys.Enter) && !gameover) // Enter  = Pause toggle
            {
                pause = !pause;
            }
            pauseState = state;

            if (state.IsKeyDown(Keys.M) && AudioState.IsKeyUp(Keys.M))
            {
                audioSetup = !audioSetup;
            }
            AudioState = state;

            if (audioSetup)
            {
                MediaPlayer.Volume = 0.5f;
            }
            else
            {
                MediaPlayer.Volume = 0.0f;
            }

            if (!pause)
            {
                if (intro)
                {
                    if (resetTimer < 30)
                    {
                        resetTimer++;
                        Character.GameOverAnimation(true);
                    }
                    else
                    {
                        if (Character.PlayerSize.X > (ScreenWidth/2))
                        {
                            Character.IntroWalking(GameSpeed);
                        }
                        else
                        {
                            MediaPlayer.Play(inGameTune);
                            intro = !intro;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < ParadeClones.Count; i++) // Update enemy movement
                    {
                        ParadeClones[i].Update(GameSpeed, GraphicsDevice, ParadeClones, ItemClones, Character.PlayerCollision.X, Character.PlayerCollision.Y);
                        if (ParadeClones[i].AllowRemove)
                        {
                            ParadeClones.Remove(ParadeClones[i]);
                            i--;
                        }
                    }

                    for (int i = 0; i < ThrowWandClones.Count; i++) // Update Wand movement
                    {
                        ThrowWandClones[i].Update(GameSpeed, GraphicsDevice);
                        if (ThrowWandClones[i].AllowRemove)
                        {
                            ThrowWandClones.Remove(ThrowWandClones[i]);
                            i--;
                        }
                    }
                    BikerGuy.Update(GameSpeed, GraphicsDevice); // Update "Torpedo" movement

                    if (!gameover)
                    {
                        AcceptExit = false;
                        HandleGameSpeed(gameTime);
                        HandleScore();
                        HandleSubClasses();

                        for (int i = 0; i < ItemClones.Count; i++) // Update item movement
                        {
                            ItemClones[i].Update(GameSpeed, GraphicsDevice);
                            if (ItemClones[i].AllowRemove)
                            {
                                ItemClones.Remove(ItemClones[i]);
                                i--;
                            }
                        }
                        Character.UpdateMovement(state, GameSpeed, GraphicsDevice);
                        BikerGuy.PlayerFound(Character.PlayerCollision.X, Character.PlayerCollision.Y);
                        gameover = Character.OutFromScreen;
                    }
                    else // If Gameover
                    {
                        MediaPlayer.Stop();
                        Character.GameOverAnimation(false);
                        HUD.UpdateGameOver(gameTime);

                        if (state.IsKeyDown(Keys.Enter))
                        {
                            SaveScores();
                            ResetValues();
                            intro = true;
                        }
                    }
                }
            }
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.SlateGray); // Asphalt color
            spriteBatch.Begin(); // Spritebatch for drawing graphics

                SBParallax.Begin(SpriteSortMode.Deferred, null, SamplerState.LinearWrap, null, null);
                    Scroll.Draw(SBParallax);  // Seperate spritebatch for scrolling backgrounds
                SBParallax.End();

                HUD.DrawStats(spriteBatch, Score, HiScore, Character.HoldingText[Character.CurrentHolding]);

                switch (GameMode)
                {
                    case false:
                        Title.Draw(spriteBatch);
                        break;
                    case true:
                    default:
                        Character.Draw(spriteBatch);
                        BikerGuy.Draw(spriteBatch);

                        for (int i = 0; i < ItemClones.Count; i++) 
                        {
                            ItemClones[i].Draw(spriteBatch);
                        }

                        for (int i = 0; i < ThrowWandClones.Count; i++) 
                        {
                            ThrowWandClones[i].Draw(spriteBatch);
                        }

                        for (int i = 0; i < ParadeClones.Count; i++)
                        {
                            ParadeClones[i].Draw(spriteBatch);
                        }

                        if (pause && !gameover)
                        {
                            HUD.DrawPause(spriteBatch);
                        }

                        if (gameover)
                        {
                            HUD.DrawGameOver(spriteBatch, Score, HiScore, ShowHighScore);
                        }
                        break;
                }
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}